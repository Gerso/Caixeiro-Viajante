/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pecaja.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class Grafo {

    private List<Cidade> cidades;
    public int QUANTIDADE_CIDADES;
    public List<Cidade> adj;
    public List<Cidade> S;
    public List<Cidade> Q;
    public List<Integer> d;
    public List<Cidade> r;
    public Aresta[][] matrizAdjacent;

    public Grafo(int qtdCidades) {
        this.QUANTIDADE_CIDADES = qtdCidades;
        cidades = new ArrayList<>();
        matrizAdjacent = new Aresta[this.QUANTIDADE_CIDADES][this.QUANTIDADE_CIDADES];
        adj = new ArrayList<>();
        S = new ArrayList<>();
        Q = new ArrayList<>();
        d = new ArrayList<>();
        r = new ArrayList<>();

        /**
         * Inciando o d[] com infinito
         */
        for (int i = 0; i < this.QUANTIDADE_CIDADES; i++) {
            d.add(Integer.MAX_VALUE);
        }

        /**
         * iniciando o r[] com vazio
         */
        for (int i = 0; i < this.QUANTIDADE_CIDADES; i++) {
            r.add(new Cidade(""));
        }

        /**
         * iniciando a matrix de adjacencia com infinito
         */
        for (int linha = 0; linha < qtdCidades; linha++) {
            for (int coluna = 0; coluna < qtdCidades; coluna++) {
                matrizAdjacent[linha][coluna] = new Aresta(null, null, Integer.MAX_VALUE);
            }
        }
    }

    public void addCidade(Cidade cidade) {
        this.cidades.add(cidade);
    }

    public void addAresta(Cidade cidade1, Cidade cidade2, int distancia) {

        int posicaoCidade1 = getCidadeIndex(cidade1);
        int posicaoCidade2 = getCidadeIndex(cidade2);

        Aresta ida = new Aresta(getCidade(cidade1.getNome()), getCidade(cidade2.getNome()), distancia);
        matrizAdjacent[posicaoCidade1][posicaoCidade2] = ida;  //ida e volta

        Aresta volta = new Aresta(getCidade(cidade2.getNome()), getCidade(cidade1.getNome()), distancia);
        matrizAdjacent[posicaoCidade2][posicaoCidade1] = volta;
    }

    /**
     *
     * @param origem
     * @param destino
     */
    public void getVizinhosDijkstra(Cidade origem, Cidade destino) {
        d.set(getCidadeIndex(origem), 0);

        S = new ArrayList<>();
        Q = new ArrayList<>();

        cidades.forEach((cidade) -> {
            Q.add(cidade);
        });

        Cidade u;

        while (!Q.isEmpty()) {

            u = extrairMinimo();
            S.add(u);
            Q.remove(u);

            System.out.println("\n----------- d -------------\n");
            d.forEach((peso) -> {
                System.out.print(peso + "| ");
            });
            System.out.println("");

            System.out.println("\n----------- r -------------\n");
            r.forEach((vertice) -> {
                System.out.print(vertice + "| ");
            });
            System.out.println("");

            System.out.println("u = " + u);

            List<Cidade> listaAdj = getAdjacentes(u);

            System.out.println("\n----------- s -------------\n");
            S.forEach((v) -> {
                System.out.print(v + "| ");
            });

            System.out.println("\n----------- lista de adjacentes de u -------------\n");
            listaAdj.forEach((vertice) -> {
                System.out.print(vertice + "| ");
            });
            System.out.println("");

            System.out.println(" ------------- RELAXAMENTO");

            for (Iterator<Cidade> iterator = listaAdj.iterator(); iterator.hasNext();) {
                Cidade next = iterator.next();
                if (listaAdj.contains(next)) {
                    relaxamento(u, next);
                    iterator.remove();
                }
            }

        }

        Cidade currentVertice = destino;
        List<Cidade> caminho = new ArrayList<>();

        System.out.println("\n---------------- caminho d ---------------");

        for (Integer double1 : d) {
            System.out.print(double1 + "| ");
        }

        System.out.println("\n---------------- caminho r ---------------");
        for (Cidade vertice : r) {
            System.out.print(vertice + "| ");
        }
        System.out.println("");

        caminho.add(destino);
        while (currentVertice != origem) {
//           System.out.println("Antes -> currente = "+currentVertice.getDescricao()+" origem = "+origem.getDescricao());
            int index = getCidadeIndex(currentVertice);
            caminho.add(r.get(index));
            currentVertice = r.get(index);
//           System.out.println("Depois -> currente = "+currentVertice.getDescricao()+" origem = "+origem.getDescricao());
        }

        Collections.reverse(caminho);

        caminho.forEach((vertice) -> {
            System.out.print("| " + vertice);
        });
        System.out.println("\ndistancia de " + origem + " para  " + destino + " e = " + d.get(getCidadeIndex(destino)));

//        System.out.println("\n---------------- caminho d ---------------");
//        for (Integer double1 : d) {
//            System.out.print(double1 + "| ");
//        }
//
//        System.out.println("\n---------------- caminho d ---------------");
//        for (Cidade vertice : r) {
//            System.out.print(vertice.getDescricao() + "| ");
//        }
//        System.out.println("");
    }

    /**
     *
     * @param u
     * @return
     */
    public List<Cidade> getAdjacentes(Cidade u) {
        List<Cidade> listAdjacents = new ArrayList<>();

        for (int linha = 0; linha < this.QUANTIDADE_CIDADES; linha++) {
            if (getCidadeIndex(u) == linha) {
                for (int coluna = 0; coluna < this.QUANTIDADE_CIDADES; coluna++) {
                    if (matrizAdjacent[linha][coluna].getPeso() < Integer.MAX_VALUE) {
                        if (!S.contains(matrizAdjacent[linha][coluna].getDestino())) {
                            listAdjacents.add(matrizAdjacent[linha][coluna].getDestino());
                        }
                    }
                }
            }

        }
        return listAdjacents;
    }

    /**
     *
     * @return
     */
    public Cidade extrairMinimo() {
        int index = 0;
        int menor = Integer.MAX_VALUE;

        for (int i = 0; i < d.size(); i++) {
            if (!S.contains(cidades.get(i))) {
                if (d.get(i) < menor) {
                    menor = d.get(i);
                    index = i;
                }
            }
        }
        return cidades.get(index);
    }

    /**
     *
     * @param u
     * @param v
     */
    public void relaxamento(Cidade u, Cidade v) {
        int indexU = getCidadeIndex(u);
        int indexV = getCidadeIndex(v);

        System.out.println("\n------------- indexs ----------------");
        System.out.println(" u = " + indexU);
        System.out.println(" v = " + indexV);

        if ((d.get(indexU) + matrizAdjacent[indexU][indexV].getPeso()) < d.get(indexV)) {

            int soma = d.get(indexU) + matrizAdjacent[indexU][indexV].getPeso();

            System.out.println("soma = " + soma);
            d.set(indexV, soma);
            r.set(indexV, u);

            System.out.println("d[ " + indexV + " ] = " + soma);
            System.out.println("r[ " + indexV + " ] = " + u);
        }
    }

    /**
     *
     */
    public void printGrafo() {
        for (int i = 0; i < this.QUANTIDADE_CIDADES; i++) {
            System.out.print(cidades.get(i));
            for (int j = 0; j < this.QUANTIDADE_CIDADES; j++) {
                System.out.print(matrizAdjacent[i][j].getPeso() == Integer.MAX_VALUE ? " |  ∞ "
                        : " | " + matrizAdjacent[i][j].getPeso());

            }
            System.out.println();
        }
    }

    /**
     *
     * @return
     */
    public boolean isAllVisited() {
        int countCid = 0;
        countCid = cidades.stream().filter((cidade) -> (cidade.isVisitado)).map((_item) -> 1).reduce(countCid, Integer::sum);
        return (countCid == QUANTIDADE_CIDADES - 1);
    }

    public int size() {
        return cidades.size();
    }

    public int getNumCidades() {
        return QUANTIDADE_CIDADES;
    }

    public void setNumCidades(int numCidades) {
        this.QUANTIDADE_CIDADES = numCidades;
    }

    public Cidade getCidade(String cidade) {
        Optional<Cidade> v = cidades.stream().filter(x -> x.getNome().equals(cidade)).findAny();
        return v.get();
    }

    public int getCidadeIndex(Cidade cidade) {
        int index = 0;

        for (int i = 0; i < cidades.size(); i++) {
            if (cidades.get(i).getNome().equals(cidade.getNome())) {
                index = i;
            }
        }
        return index;
    }

    public List<Cidade> getCidades() {
        return cidades;
    }

    public void setCidades(List<Cidade> cidades) {
        this.cidades = cidades;
    }

}
