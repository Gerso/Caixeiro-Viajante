/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pecaja.models;

public class NewMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Grafo grafo = new Grafo(7);
        
//        grafo.incluirVertice("Apodi");
//        grafo.incluirVertice("Severiano Melo");
//        grafo.incluirVertice("Caraubas");
//        grafo.incluirVertice("Olho dágua do Borges");
//        grafo.incluirVertice("Rafael Godeiro");
//        grafo.incluirVertice("Patu");
//        grafo.incluirVertice("Almino Afonso");
//        grafo.incluirVertice("Frutuoso Gomes");
//        grafo.incluirVertice("Serrinha dos Pintos");
//        grafo.incluirVertice("Martins");
//        grafo.incluirVertice("Umarizal");
//        grafo.incluirVertice("Riacho da Cruz");
//        grafo.incluirVertice("Portalegre");
//        grafo.incluirVertice("Pau dos Ferros");
//        grafo.incluirVertice("Tabuleiro Grande");
//        
//        grafo.incluirAresta("Apodi","Severiano Melo", 29.3);
//        grafo.incluirAresta("Severiano Melo","Tabuleiro Grande", 25.7);
//        grafo.incluirAresta("Severiano Melo", "Riacho da Cruz", 25.3);
//        grafo.incluirAresta("Tabuleiro Grande", "Pau dos Ferros", 35.6);
//        grafo.incluirAresta("Pau dos Ferros", "Portalegre", 34.3);
//        grafo.incluirAresta("Pau dos Ferros", "Serrinha dos Pintos", 43.9);

        Cidade a = new Cidade("A");
        Cidade b = new Cidade("B");
        Cidade c = new Cidade("C");
        Cidade d = new Cidade("D");
        Cidade e = new Cidade("E");
        Cidade f = new Cidade("F");
        Cidade g = new Cidade("G");
        
        grafo.addCidade(a);
        grafo.addCidade(b);
        grafo.addCidade(c);
        grafo.addCidade(d);
        grafo.addCidade(e);
        grafo.addCidade(f);
        grafo.addCidade(g);
        
        grafo.addAresta(a, b, 6);
        grafo.addAresta(a, c, 5);
        grafo.addAresta(a, e, 1);

        grafo.addAresta(b, d, 3);

        grafo.addAresta(c, d, 4);
        grafo.addAresta(c, f, 8);
        grafo.addAresta(c, g, 7);

        grafo.addAresta(d, g, 2);

        grafo.addAresta(f, e, 13);

        grafo.addAresta(g, d, 2);
        
        grafo.printGrafo();
        
        //grafo.getVizinhosProximos(c);
        grafo.getVizinhosDijkstra(b,f);
        
    }
    
}
