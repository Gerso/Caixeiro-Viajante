/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pecaja.models;

public class Aresta {

    private int peso;
    private Cidade origem;
    private Cidade destino;

    public Aresta(Cidade v1, Cidade v2, int peso) {
        this.peso = peso;
        this.origem = v1;
        this.destino = v2;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }

    public int getPeso() {
        return peso;
    }

    public void setDestino(Cidade destino) {
        this.destino = destino;
    }

    public Cidade getDestino() {
        return destino;
    }

    public void setOrigem(Cidade origem) {
        this.origem = origem;
    }

    public Cidade getOrigem() {
        return origem;
    }

}
