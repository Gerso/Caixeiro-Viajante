/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.pecaja.models;

public class Cidade {

    private String nome;
    boolean isVisitado;

    public Cidade() {
    }

    public Cidade(String descricao) {
        this.nome = descricao;
    }

    public Cidade(String nome, boolean isVisitado) {
        this.nome = nome;
        this.isVisitado = isVisitado;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean isIsVisitado() {
        return isVisitado;
    }

    public void setIsVisitado(boolean isVisitado) {
        this.isVisitado = isVisitado;
    }

    @Override
    public String toString() {
        return nome;
    }

}
